module Rails
	module Heatmap
		class Color
			attr_accessor :start_color
			attr_accessor :end_color

			def initialize(color = '#FF0000')
				@start_color = "#000000"
				if ! is_color?(color) 
					@end_color = "#FF0000"
				else
					@end_color = color
				end
				
			end
			
			def is_color?(color)
				case (/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/ =~ color)
					when 0 then true
					when nil then false
					else  false
				end
			end
			
			def set_start_color(color)
				@start_color = color
			end
			
			def get_start_color()
				@start_color
			end
			
			def set_end_color(color)l
				@end_color = color
			end
			
			def get_end_color()
				@end_color
			end
			
			def colors
				@colors = Array.new
				128.times do |clr_index|
					@colors[clr_index] = '#000'
				end
				@colors = build_colors
			end
			
			def match_color(percent)
				if @colors.nil?
					@colors = build_colors
				end
				if percent >= 0 
					percent = percent.to_f / 100.0
				end
				puts "Percent"
				puts percent
				@colors[(128 * percent).floor.to_i]
			end
 
private

			def build_colors
				colors = []
				start_red = 0
				start_green = 0
				start_blue = 0
				end_red = 255
				end_green = 0
				end_blue = 0
				case @start_color.length - 1
					when 3 then 
						start_red = @start_color[1]
						start_green = @start_color[2]
						start_blue = @start_color[3]
					when 6 then
						start_red = @start_color[1,2]
						start_green = @start_color[3,2]
						start_blue = @start_color[5,2]
					end
				case @end_color.length - 1
					when 3 then
						end_red = @end_color[1]
						end_green = @end_color[2]
						end_blue = @end_color[3]
					when 6 then
						end_red = @end_color[1,2]
						end_green = @end_color[3,2]
						end_blue = @end_color[5,2]
					end
				128.times do |step|
					red = (end_red.to_i(16) - start_red.to_i(16)) * (step.to_f / 128.0) + start_red.to_i(16)
					green = (end_green.to_i(16) - start_green.to_i(16)) * (step.to_f / 128.0) + start_green.to_i(16)
					blue = (end_blue.to_i(16) - start_blue.to_i(16)) * (step.to_f / 128.0) + start_blue.to_i(16)
					colors[step] ="#" + red.to_i.ceil.to_s(16) + green.to_i.ceil.to_s(16) + blue.to_i.ceil.to_s(16)
				end
				return colors
			end
			
		end #end color class
	end #end module heatmap
end #end module rails