require '~/gems/rails-heatmap/lib/rails/heatmap/param_helpers.rb'
require '~/gems/rails-heatmap/lib/rails/heatmap/datatable.rb'

row = []
rows  = []

data = Rails::Heatmap::DataTable.new()

data.new_column('string','segment')
data.new_column('number','area1')
data.new_column('number','area2')
data.new_column('number','area3')
data.new_column('number','area4')
data.new_column('number','area5')
data.new_column('number','area6')
data.new_column('number','area7')

7.times do |col|
	row[0] = 'Starting'
	row[1] = col * 1.5
	row[1] = col * 2.5
	row[1] = col * 3.5
	row[1] = col * 4.5
	row[1] = col * 5.5
	row[1] = col * 6.5
	row[1] = col * 7.5

	rows << row
	row = []
end

data.add_rows(rows)

puts "Data Table Width: #{data.dt_width.to_s}"
puts "Data Table Height: #{data.dt_height.to_s}"
puts "Overall Area: #{(data.dt_width  * data.dt_height).to_s}"
	
	
	
