require 'spec_helper'

describe Rails::Heatmap do
	clr = Rails::Heatmap::Color.new() rescue nil
	
	
	it 'should set a default color if one is not specified' do 
		expect(clr.get_end_color()).to eq('#FF0000')
	end
	
	it 'should allow a end color to be specified through init' do 
		clr = Rails::Heatmap::Color.new('#00FF00')
		expect(clr.get_end_color).to eq('#00FF00')
	end
	
	it 'should allow overridding of start color' do 
		clr.set_start_color('#0000FF')
		expect(clr.get_start_color).to eq('#0000FF')
	end
	
	it 'should return a 128 element array of color gradient' do
		expect(clr.colors.length).to be(128)
		expect(clr.colors[0]).to eq("#00ff")
		expect(clr.colors[clr.colors.length - 1]).to eq('#0fd1')
	end
	
	it 'should validate a color' do 
		expect(clr.is_color?('#FFFFFF')).to be(true)
		expect(clr.is_color?('#111')).to be(true)
		expect(clr.is_color?('#G11')).to be(false)
	end

end