require 'spec_helper'

describe Rails::Heatmap do
	it 'should have a version number' do
		expect(Rails::Heatmap::VERSION).to_not be_nil
	end

	it 'should allow creation of a new data table' do
		expect(Rails::Heatmap::DataTable.new).to be_instance_of(Rails::Heatmap::DataTable)
	end

	it 'should return the width and height of a data table' do 
		data = Rails::Heatmap::DataTable.new
		expect(data.dt_width).to be(0)
		expect(data.dt_height).to be(0)
	end
	
	it 'should add columns and rows' do 
		data = Rails::Heatmap::DataTable.new
		data.new_column('string', 'test')
		data.new_column('number','first')
		data.add_rows([["Example", 1]])
		expect(data.dt_width).to be(2)
		expect(data.dt_height).to be(1)
	end
end