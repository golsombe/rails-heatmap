# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails/heatmap/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-heatmap"
  spec.version       = Rails::Heatmap::VERSION
  spec.authors       = ["Robert Hall"]
  spec.email         = ["golsombe@gmail.com"]
  spec.summary       = %q{Create SVG based MosaicPlot Heatmaps for HTML5}
  spec.description   = %q{Additionally can create static assets for other projects.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "guard"
  spec.add_development_dependency "guard-rspec"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "pry-remote"
  spec.add_development_dependency "pry-nav"
  spec.add_dependency "rasem"
end
